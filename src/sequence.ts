import {inject} from '@loopback/context';
import {
  FindRoute,
  InvokeMethod,
  ParseParams,
  Reject,
  RequestContext,
  RestBindings,
  Send,
  SequenceHandler,
} from '@loopback/rest';

const jwt = require('jsonwebtoken')
var secret = "nM%/L&|pwW(k%?uamc5~/(O:-+syGv"

const SequenceActions = RestBindings.SequenceActions;

export class MySequence implements SequenceHandler {
  constructor(
    @inject(SequenceActions.FIND_ROUTE) protected findRoute: FindRoute,
    @inject(SequenceActions.PARSE_PARAMS) protected parseParams: ParseParams,
    @inject(SequenceActions.INVOKE_METHOD) protected invoke: InvokeMethod,
    @inject(SequenceActions.SEND) public send: Send,
    @inject(SequenceActions.REJECT) public reject: Reject,
  ) {}

  async handle(context: RequestContext) {
    try {
      const {request, response} = context;
      const route = this.findRoute(request);
      const args = await this.parseParams(request, route);
      const result = await this.invoke(route, args);

      if(request.path.includes('/authorize')){
        this.send(response, result);
      }

      if(request.path.includes('/accounts') && request.method == "POST"){
        this.send(response, result);
      }

      if(!request.headers.authorization){
        response.json({error: "No Token", message: "You did not provide an authentication header"})
        this.reject(context, new Error("No Token"))
      }
      
      const token = jwt.verify(request.headers.authorization, secret)
      
      if(token){
        if(request.originalUrl.includes("/admin")){
          if(!token.isAdmin){
            response.json({error: "Unauthorized", message: "Your token does not have security clearence for this route"})
            this.reject(context, new Error("Invalid Permissions"))
          }else{
            this.send(response, result);
          }
        }else{
          this.send(response, result);
        }
      }else{
        response.json({error: "No Token", message: "You provided an invalid authentication token"})
        this.reject(context, new Error("Invalid Token"))

      }
    } catch (err) {
      this.reject(context, err);
    }
  }
}
