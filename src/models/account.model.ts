import {Entity, model, property, ArrayType} from '@loopback/repository';
//Inferno was here.
@model({settings: {}})
export class Account extends Entity {
  @property({
    type: 'number',
    id: true
  })
  id: number;

  @property({
    type: 'string',
    required: true
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    unique: true
  })
  username: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true
  })
  password: string;

  @property({
    type: 'string',
    required: true
  })
  token: string;


  @property({
    type: 'boolean',
    required: true,
    default: false,
  })
  verified: boolean;

  @property({
    type: 'boolean',
    required: true,
    default: false,
  })
  isAdmin: boolean;

  constructor(data?: Partial<Account>) {
    super(data);
  }
}
