import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Post extends Entity {
  @property({
    type: 'number',
    id: true
  })
  id: number;
  
  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  author: string;

  @property({
    type: 'string',
    required: true,
  })
  content: string;

  @property({
    type: 'number',
    required: true,
    default: 0,
  })
  likes: number;

  @property({
    type: 'number',
    required: true,
    default: 0,
  })
  dislikes: number;


  constructor(data?: Partial<Post>) {
    super(data);
  }
}
