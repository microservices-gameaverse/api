import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
  ArrayType,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  HttpErrors
} from '@loopback/rest';


const bcrypt = require('bcrypt-node')
import { Account } from '../models';
import { AccountRepository } from '../repositories';
import { userInfo } from 'os';
const jwt = require('jsonwebtoken')

var secret = "nM%/L&|pwW(k%?uamc5~/(O:-+syGv"

type ErrorResponse = {
  error: number, //fuck normal languages
  message: string
}

type meRequest = {
  token: string
}


type AccountCreationRequest = {
  /*
    The base type for a request to create an account. Different from account type because
    verified and id are auto generated in the api backend and only user info is provided by user
  */
  // ip: string, // May or may not track you. I may to sell ips to big companies (FaceBook).
  name: string,
  username: string,
  email: string,
  password: string
}

type FormattedAccount = {
  id: number,
  name: string,
  username: string,
  email: string,
  verified: boolean,
  isAdmin: boolean
}


export class AccountControllerController {
  constructor(
    @repository(AccountRepository)
    public accountRepository: AccountRepository,
  ) { }

  @post('/accounts', {
    responses: {
      '200': {
        description: 'Account model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Account } } },
      },
    },
  })

  async create(@requestBody() account: AccountCreationRequest): Promise<Account | ErrorResponse> {
    const entries = await this.accountRepository.count()

    var users = await this.accountRepository.find({where: {username: account.username}})
    if(users[0]){
      return {
        error: 409,
        message: "A user with your username already exists"
      }
    }
    var salt = bcrypt.genSaltSync(10);
    const password = bcrypt.hashSync(account.password, salt)

    const newAccount = {
      id: entries.count + 1,
      name: account.name,
      username: account.username,
      email: account.email,
      password: password,
      verified: false,
      isAdmin: false,
      token: jwt.sign({name: account.username, isAdmin: false}, secret)
    }
    
    return await this.accountRepository.create(newAccount);
  }

  @post('/authorize', {
    responses: {
      '200': {
        description: 'Account model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Account } } },
      },
    },
  })
  async authorize(@requestBody() userdata: {username: string, password: string}): Promise<Account | ErrorResponse>{
    const user =  await this.accountRepository.find({where:{username: userdata.username}});
    if(!user[0]) return { // Will return ErrorResponse with a number value of 404 when no user is found in ther database of choice.
      error: 404,
      message: `The user was not found`
    }

    if(bcrypt.compareSync(userdata.password, user[0].password)){
      return user[0]
    }else{
      return { // When passwords do not match it will return the ErrorResponse with a number of 401.
        error: 401,
        message: `The passwords didnt match`
      }
    }

  }



  @get('/accounts/count', {
    responses: {
      '200': {
        description: 'Account model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Account)) where?: Where,
  ): Promise<Count> {
    return await this.accountRepository.count(where);
  }

  @get('/accounts', {
    responses: {
      '200': {
        description: 'Array of Account model instances',
        content: {
          'application/json': {
            schema: { type: 'array'},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Account)) filter?: Filter,
  ): Promise<FormattedAccount[]> {
    const users = await this.accountRepository.find(filter);
    var formattedUserObject: FormattedAccount[] = [];
    await users.forEach(function(user){ // To shift throught user to get users that match closer to what is being looked for.
      formattedUserObject.push({
        id: user.id,
        name: user.name,
        username: user.username,
        email: user.email,
        verified: user.verified,
        isAdmin: user.isAdmin
      })
    })

    return formattedUserObject
  }



  @get('/accounts/me', {
    responses: {
      '200': {
        description: 'Get The Currently Logged In Users Account',
        content: {
          'application/json': {
            schema: { type: 'object'},
          },
        },
      },
    },
  })
  async get_me(
    @requestBody() me_request: meRequest,
  ): Promise<Account> {
    const users =  await this.accountRepository.find({where: {token: me_request.token}});
    return users[0]
  }



  @patch('/admin/accounts', {
    responses: {
      '200': {
        description: 'Account PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody() account: Account,
    @param.query.object('where', getWhereSchemaFor(Account)) where?: Where,
  ): Promise<Count> {
    return await this.accountRepository.updateAll(account, where);
  }

  @get('/accounts/{id}', {
    responses: {
      '200': {
        description: 'Account model instance',
        content: { 'application/json': { schema: { 'x-ts-type': Account } } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Account> {
    return await this.accountRepository.findById(id);
  }

  @patch('/admin/accounts/{id}', {
    responses: {
      '204': {
        description: 'Account PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() account: Account,
  ): Promise<void> {
    await this.accountRepository.updateById(id, account);
  }

  @put('/admin/accounts/{id}', {
    responses: {
      '204': {
        description: 'Account PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() account: Account,
  ): Promise<void> {
    await this.accountRepository.replaceById(id, account);
  }

  @del('/admin/accounts/{id}', {
    responses: {
      '204': {
        description: 'Account DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.accountRepository.deleteById(id);
  }
}
